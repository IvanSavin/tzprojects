<?php
require_once ('db/connect.php');

class zadachi extends dbconnect
{
    private $conn;

    public function __construct() {
        $dbcon = new parent();

        $this->conn = $dbcon->connect();
    }


    /*
     * $num = number of entries in 1 page
     * $pages - total number of pages
     * if url parameters are not empty, forms a string of parameters
     * returns pagination and sort data
    **/
    function getData($page, $sort, $order)
    {
        $num = 3;

        $countPage = $this->conn->query('SELECT COUNT(*) FROM zadachi')->fetchColumn();

        $pages = intval($countPage / $num) + 1;
        $page = intval($page);

        if($page < 0)
            $page = 1;
        if($page > $pages)
            $page = $pages;

        $start = $page * $num - $num;

        $statement = $this->conn->prepare("SELECT * FROM zadachi ORDER BY " . $sort  . " " . $order . " LIMIT " . $start . ", " . $num);

        $statement->execute();

        $arResult = $statement->fetchAll(PDO::FETCH_ASSOC);


        if($sort == 'id')
            $stringNav = $this->getNav($page, $pages, '?');
        else
            $stringNav = $this->getNav($page, $pages, "?sort=$sort&order=$order&");


        return [ $arResult, $stringNav ];

    }

    /*
     * checks if the forward or back pages are needed
     */
    function getNav($page, $pages, $sort)
    {
        $pervpage = '';
        $nextpage = '';
        $page2left = '';
        $page1left = '';
        $page2right = '';
        $page1right = '';

        if ($page != 1)
            $pervpage = '<a href= '. $sort . 'page=1> << </a>
                               <a href= '. $sort . 'page='. ($page - 1) .'> < </a> ';

        if ($page != $pages)
            $nextpage = ' <a href= '. $sort . 'page='. ($page + 1) .'>></a>
                                   <a href= '. $sort . 'page=' .$pages. '>>></a>';


        if($page - 2 > 0)
            $page2left = ' <a href= '. $sort . 'page='. ($page - 2) .'>'. ($page - 2) .'</a> | ';
        if($page - 1 > 0)
            $page1left = '<a href= '. $sort . 'page='. ($page - 1) .'>'. ($page - 1) .'</a> | ';
        if($page + 2 <= $pages)
            $page2right = ' | <a href= '. $sort . 'page='. ($page + 2) .'>'. ($page + 2) .'</a>';
        if($page + 1 <= $pages)
            $page1right = ' | <a href= '. $sort . 'page='. ($page + 1) .'>'. ($page + 1) .'</a>';


        $navString = $pervpage.$page2left.$page1left.'<b>'.$page.'</b>'.$page1right.$page2right.$nextpage;

        return $navString;
    }

    function insertElem($userName, $email, $text)
    {
        $statement = $this->conn->prepare("INSERT INTO zadachi (userName, email, text) VALUES (:userName, :email, :text)");

        $statement->bindParam( ":userName", $userName);
        $statement->bindParam( ":email", $email);
        $statement->bindParam( ":text", $text);

        if($statement->execute())
            return 'Ваша задача добавлена!';
        else
            return 'Произошла ошибка!';

    }

    function updateElem($id, $userName, $email, $text, $status)
    {
        if($status == 'on')
            $status = 'отредактировано администратором Выполнено';
        else
            $status = 'отредактировано администратором';



        $statement = $this->conn->prepare("UPDATE zadachi SET userName = :userName, email = :email, text = :text, status = :status WHERE id = :id");

        $statement->execute(array(":id" => $id, ":userName" => $userName, ":email" => $email, ":text" =>  $text, ":status" => $status));

        if($statement->execute(array(":id" => $id, ":userName" => $userName, ":email" => $email, ":text" =>  $text, ":status" => $status)))
            return 'Запись отредактирована!';
        else
            return 'Произошла ошибка!';
    }

}


