<?php
require_once ('../db/connect.php');

class auth extends dbconnect
{
    private $conn;

    public function __construct() {
        $dbcon = new parent();

        $this->conn = $dbcon->connect();
    }

    function authUser($login, $password)
    {
        $statement = $this->conn->prepare("SELECT * FROM users");

        $statement->execute();

        $arResult = $statement->fetchAll(PDO::FETCH_ASSOC);

        if ($arResult[0]['userName'] == $login && $arResult[0]['password'] == $password) {
            $_SESSION['auth'] = 1;
            return 'succes';
        } else
            return 'Не верные данные';
    }
}