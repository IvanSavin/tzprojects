<?php
session_start();
require_once("../models/authModel.php");
require_once("../shablon/header.php");

$auth = new auth();

if(!empty($_POST))
{
    $userName = htmlspecialchars($_POST['userName']);
    $password = htmlspecialchars($_POST['password']);

    $answer = $auth->authUser($userName, $password);
    ?>
    <script>
        window.onload = function()
        {
            $("#modal").modal('show');
        }
    </script>
    <?
}

if(!empty($_SESSION['auth']) && $_SESSION['auth'] == 1)
{
    header('Location: ..', true);
    die();
}

include "../views/authView.php";
require_once("../shablon/footer.php");