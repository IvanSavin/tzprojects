<?php
session_start();
require_once("models/zadachiModel.php");
require_once("shablon/header.php");

$zad = new zadachi();

if(isset($_GET['login']) && $_GET['login'] == 'out')
    $_SESSION['auth'] = 0;



if(!empty($_POST['id']))
{
    if($_SESSION['auth'] == 1) {
        $id = htmlspecialchars($_POST['id']);
        $userName = htmlspecialchars($_POST['userName']);
        $email = htmlspecialchars($_POST['email']);
        $text = htmlspecialchars($_POST['text']);
        $status = htmlspecialchars($_POST['status']);

        $answer = $zad->updateElem($id, $userName, $email, $text, $status);

        ?>
        <script>
            window.onload = function () {
                $("#modal").modal('show');
            }
        </script>
        <?
    }
    else
    {
        $answer = 'Пожалуйста, авторизуйтесь';
        ?>
        <script>
            window.onload = function()
            {
                $("#modal").modal('show');
            }
        </script>
        <?
    }
}
elseif(!empty($_POST))
{
    $userName = htmlspecialchars($_POST['userName']);
    $email = htmlspecialchars($_POST['email']);
    $text = htmlspecialchars($_POST['text']);

    $answer = $zad->insertElem($userName, $email, $text);
    ?>
    <script>
        window.onload = function()
        {
            $("#modal").modal('show');
        }
    </script>
    <?
}

$page = empty($_GET['page']) ? 1 : $_GET['page'];
$sort = empty($_GET['sort']) ? 'id' : $_GET['sort'];
$order = empty($_GET['order']) ? 'ASC' : $_GET['order'];

/*
 * getData return data array and pagination string
 */
$allData = $zad->getData($page, $sort, $order);
$arResult = $allData[0];
$stringNav = $allData[1];

/*
 * for the link in the menu
 */
if(empty($_GET['page']))
{
    if(empty($_GET['order']) || $_GET['order'] == 'ASC')
        $order = '&order=DESC';
    else
        $order = '&order=ASC';
}
else
{
    if(empty($_GET['order']) || $_GET['order'] == 'ASC')
        $order = '&order=DESC&page='.$page;
    else
        $order = '&order=ASC&page='.$page;
}

include "views/zadachiView.php";
require_once("shablon/footer.php");