<?php

if (isset($_SESSION['auth']) && $_SESSION['auth'] == 1): ?>
    <div class="container">
        <a href="?login=out">Выход</a>
        <table class="table">
            <thead class="thead-inverse">
                <tr>
                    <th>#</th>
                    <th>Имя пользователя</th>
                    <th>Email</th>
                    <th>Text</th>
                    <th>Status</th>
                </tr>
            </thead>

            <thead>
            <tr>
                <th></th>
                <th><a href="?sort=userName<?=$order?>">Имя пользователя</a></th>
                <th><a href="?sort=email<?=$order?>">Email</a></th>
                <th><a href="?sort=status<?=$order?>">Status</a></th>
            </tr>
            </thead>

            <tbody>

            <? $i = 1; ?>
            <?php foreach ($arResult as $item): ?>
                <form method="POST" action="">
                    <input type="hidden" value="<?=$item['id']?>" name = "id">
                    <tr>
                        <th scope="row"><?=$i; $i++;?></th>
                        <td><input type="text" value = "<?=$item['userName']?>" name = "userName"></td>
                        <td><input type="text" value = "<?=$item['email']?>" name = "email"></td>
                        <td><input type="text" value = "<?=$item['text']?>" name = "text"></td>
                        <td>
                            <label class="lableCheck">выполнено: </label>
                            <input type="checkbox" name = "status" <? if(preg_match('/(Выполнено)/', $item['status'])) echo 'checked';?>>
                            <br>
                            <? if(preg_match('/(отредактировано администратором)/', $item['status'])) echo 'отредактировано администратором';?>
                        </td>
                        <td><button type="submit">Submit</button></td>
                    </tr>
                </form>

            <? endforeach; ?>

            </tbody>
        </table>
        <? echo $stringNav; ?>
    </div>
<? else: ?>
    <div class="container">
        <a href="auth/">Войти</a>
        <table class="table">
            <thead class="thead-inverse">
                <tr>
                    <th>#</th>
                    <th>Имя пользователя</th>
                    <th>Email</th>
                    <th>Text</th>
                    <th>Status</th>
                </tr>
            </thead>

            <tbody>

            <? $i = 1; ?>
                <?php foreach ($arResult as $item): ?>

                    <tr>
                        <th scope="row"><?=$i; $i++;?></th>
                        <td><?=$item['userName']?></td>
                        <td><?=$item['email']?></td>
                        <td><?=$item['text']?></td>
                        <td>
                            <label class="lableCheck">выполнено: </label>
                            <input type="checkbox" name="status" disabled <? if(preg_match('/(Выполнено)/', $item['status'])) echo 'checked';?>>
                            <br>
                            <? if(preg_match('/(отредактировано администратором)/', $item['status'])) echo 'отредактировано администратором';?>
                        </td>
                    </tr>

                <? endforeach; ?>

            </tbody>
        </table>
        <? echo $stringNav; ?>
    </div>
<? endif; ?>


<div class="container">
    <H1 style="margin-top: 15px">Добавить задачу</H1>
    <form method="POST" action="">
        <table class="table">
            <thead class="thead-inverse">
            <tr>
                <th>Имя пользователя</th>
                <th>Email</th>
                <th>Text</th>
            </tr>
            </thead>

            <tbody>
                <tr>
                    <td><input required type="text" name="userName"></td>
                    <td><input required type="email" name="email"></td>
                    <td><textarea required  type="text" name="text"></textarea></td>
                </tr>
            </tbody>
        </table>
        <button type="submit">Submit</button>
    </form>
</div>


<div class="modal fade" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Спасибо!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?=$answer?></p>
            </div>
        </div>
    </div>
</div>

